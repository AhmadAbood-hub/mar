package com.example.myapplicationaa.model;

public class Product {

    public int id;
    public String picture;
    public String number;
    public int price_befor;
    public int price_after;
    public String name_ar;
    public String name_en;
    public String name_gr;
    public String edibility_ar;
    public String edibility_en;
    public String edibility_gr;
    public String description_ar;
    public String description_en;
    public String description_gr;
    public String detail_ar;
    public String detail_en;
    public String detail_gr;

    public String communication_ar;
    public String communication_en;
    public String communication_gr;

    public String other_ar;
    public String other_en;
    public String other_gr;

    public String alimentrly_value_ar;
    public String alimentrly_value_en;
    public String alimentrly_value_gr;

    public int discount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getPrice_befor() {
        return price_befor;
    }

    public void setPrice_befor(int price_befor) {
        this.price_befor = price_befor;
    }

    public int getPrice_after() {
        return price_after;
    }

    public void setPrice_after(int price_after) {
        this.price_after = price_after;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_gr() {
        return name_gr;
    }

    public void setName_gr(String name_gr) {
        this.name_gr = name_gr;
    }

    public String getEdibility_ar() {
        return edibility_ar;
    }

    public void setEdibility_ar(String edibility_ar) {
        this.edibility_ar = edibility_ar;
    }

    public String getEdibility_en() {
        return edibility_en;
    }

    public void setEdibility_en(String edibility_en) {
        this.edibility_en = edibility_en;
    }

    public String getEdibility_gr() {
        return edibility_gr;
    }

    public void setEdibility_gr(String edibility_gr) {
        this.edibility_gr = edibility_gr;
    }

    public String getDescription_ar() {
        return description_ar;
    }

    public void setDescription_ar(String description_ar) {
        this.description_ar = description_ar;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getDescription_gr() {
        return description_gr;
    }

    public void setDescription_gr(String description_gr) {
        this.description_gr = description_gr;
    }

    public String getDetail_ar() {
        return detail_ar;
    }

    public void setDetail_ar(String detail_ar) {
        this.detail_ar = detail_ar;
    }

    public String getDetail_en() {
        return detail_en;
    }

    public void setDetail_en(String detail_en) {
        this.detail_en = detail_en;
    }

    public String getDetail_gr() {
        return detail_gr;
    }

    public void setDetail_gr(String detail_gr) {
        this.detail_gr = detail_gr;
    }

    public String getCommunication_ar() {
        return communication_ar;
    }

    public void setCommunication_ar(String communication_ar) {
        this.communication_ar = communication_ar;
    }

    public String getCommunication_en() {
        return communication_en;
    }

    public void setCommunication_en(String communication_en) {
        this.communication_en = communication_en;
    }

    public String getCommunication_gr() {
        return communication_gr;
    }

    public void setCommunication_gr(String communication_gr) {
        this.communication_gr = communication_gr;
    }

    public String getOther_ar() {
        return other_ar;
    }

    public void setOther_ar(String other_ar) {
        this.other_ar = other_ar;
    }

    public String getOther_en() {
        return other_en;
    }

    public void setOther_en(String other_en) {
        this.other_en = other_en;
    }

    public String getOther_gr() {
        return other_gr;
    }

    public void setOther_gr(String other_gr) {
        this.other_gr = other_gr;
    }

    public String getAlimentrly_value_ar() {
        return alimentrly_value_ar;
    }

    public void setAlimentrly_value_ar(String alimentrly_value_ar) {
        this.alimentrly_value_ar = alimentrly_value_ar;
    }

    public String getAlimentrly_value_en() {
        return alimentrly_value_en;
    }

    public void setAlimentrly_value_en(String alimentrly_value_en) {
        this.alimentrly_value_en = alimentrly_value_en;
    }

    public String getAlimentrly_value_gr() {
        return alimentrly_value_gr;
    }

    public void setAlimentrly_value_gr(String alimentrly_value_gr) {
        this.alimentrly_value_gr = alimentrly_value_gr;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
