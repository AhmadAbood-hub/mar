package com.example.myapplicationaa.model;

public class Offers {
    public int id;
    public String detail_ar;
    public String detail_en;
    public String detail_gr;
    public String picture;
    public float diccount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public float getDiccount() {
        return diccount;
    }

    public void setDiccount(float diccount) {
        this.diccount = diccount;
    }
}
