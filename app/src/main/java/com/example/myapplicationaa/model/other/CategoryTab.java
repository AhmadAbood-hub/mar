package com.example.myapplicationaa.model.other;

public class CategoryTab {
    String name;
    int id;
    Boolean click;


    public CategoryTab(String name,int id) {
        this.name = name;
        this.id = id;
        click = false;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getClick() {
        return click;
    }

    public void setClick(Boolean click) {
        this.click = click;
    }


}
