package com.example.myapplicationaa.model;

import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductORCategory {
    String type;
    List<Product> data_product = new ArrayList<>();
    List<Categories> data_category = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Product> getData_product() {
        return data_product;
    }

    public void setData_product(List<Product> data_product) {
        this.data_product = data_product;
    }

    public List<Categories> getData_category() {
        return data_category;
    }

    public void setData_category(List<Categories> data_category) {
        this.data_category = data_category;
    }
}
