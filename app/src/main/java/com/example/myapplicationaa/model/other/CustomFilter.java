package com.example.myapplicationaa.model.other;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Filter;

import com.example.myapplicationaa.adapter.AllProductAdapter;
import com.example.myapplicationaa.adapter.MainProductAdapter;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.allProductAdapter;
import static com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment.productAdapter;

public class CustomFilter extends Filter {

    MainProductAdapter adapter;
    AllProductAdapter Alladapter;
    ArrayList<Product> filterList;
    SharedPreferences sp;
    Context context;

    public CustomFilter(ArrayList<Product> filterList, MainProductAdapter adapter, Context context) {
        this.adapter = adapter;
        this.filterList = filterList;
        this.context = context;

    }

    public CustomFilter(ArrayList<Product> filterList, AllProductAdapter Alladapter, Context context) {
        this.Alladapter = Alladapter;
        this.filterList = filterList;
        this.context = context;

    }

    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);

        int language = sp.getInt("language", 0);

        FilterResults results = new FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if (constraint != null && constraint.length() > 0) {
            //CHANGE TO UPPER
            constraint = constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Product> filteredPets = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                //CHECK

                if (language == 1) {
                    if (filterList.get(i).name_ar.toUpperCase().contains(constraint)) {
                        //ADD PLAYER TO FILTERED PLAYERS
                        filteredPets.add(filterList.get(i));
                    }
                } else if (language == 0) {

                    if (filterList.get(i).name_en.toUpperCase().contains(constraint)) {
                        //ADD PLAYER TO FILTERED PLAYERS
                        filteredPets.add(filterList.get(i));
                    }

                }



            }

            results.count = filteredPets.size();
            results.values = filteredPets;

        } else {
            results.count = filterList.size();
            results.values = filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        if (!MainActivity.filter) {
            adapter.mProducts = (ArrayList<Product>) results.values;
            adapter.notifyDataSetChanged();
        } else {
            Alladapter.products = (ArrayList<Product>) results.values;
            Alladapter.notifyDataSetChanged();
        }


    }
}