package com.example.myapplicationaa.model;

public class User {

    public int id;
    public String name_en;
    public String name_ar;
    public String name_gr;

    public String email;
    public String password;

    public String first_name_en;
    public String first_name_ar;
    public String first_name_gr;

    public String last_name_en;
    public String last_name_ar;
    public String last_name_gr;

    public String phone;
    public String prefecture;
    public String country;
    public String postcard;
    public String city;
    public String num_building;
    public String num_street;
    public int points;

}
