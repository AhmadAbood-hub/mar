package com.example.myapplicationaa.view.Activites;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.other.JavaMailAPI;
import com.example.myapplicationaa.view.Activites.create_new_user.CreateNewUserActivity;
import com.example.myapplicationaa.view.Activites.login.LoginActivity;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public class VerifayEmailActivity extends AppCompatActivity {

    EditText edtEmailSignUp;
    EditText edtPasswordSignUp;
    EditText edtConfirmPasswordSignUp;

    Button btnSignUp;
    String code;

    private AlertDialog.Builder alertDialog;

    public SharedPreferences sp;

    int language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifay_email);


        sp = getSharedPreferences("THIS", MODE_PRIVATE);

        language = sp.getInt("language", 0);

        edtEmailSignUp = findViewById(R.id.edtEmailSignUp);
        edtPasswordSignUp = findViewById(R.id.edtPasswordSignUp);
        edtConfirmPasswordSignUp = findViewById(R.id.edtConfirmPasswordSignUp);

        btnSignUp = findViewById(R.id.btnSignUp);

        Random rand = new Random();

        String a = String.valueOf(rand.nextInt(10));
        String b = String.valueOf(rand.nextInt(10));
        String c = String.valueOf(rand.nextInt(10));
        String d = String.valueOf(rand.nextInt(10));

        code = a + b + c + d;


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Email = edtEmailSignUp.getText().toString();
                String Password = edtPasswordSignUp.getText().toString();
                String Password2 = edtConfirmPasswordSignUp.getText().toString();

                if (Email.isEmpty()) {

                    if (language == 1) {
                        edtEmailSignUp.setError("يرجى ادخال الايميل");

                    } else if (language == 0) {
                        edtEmailSignUp.setError("Email Requierd");
                    }
                    edtEmailSignUp.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {

                    if (language == 1) {
                        edtEmailSignUp.setError("كلمة السر 6 عناصر");

                    } else if (language == 0) {
                        edtEmailSignUp.setError("Vaild Email Requierd");
                    }
                    edtEmailSignUp.requestFocus();
                    return;
                }

                if (Password.isEmpty() || Password.length() < 6) {
                    if (language == 1) {
                        edtPasswordSignUp.setError("كلمة السر 6 عناصر");

                    } else if (language == 0) {
                        edtPasswordSignUp.setError("6 char password required");
                    }
                    edtPasswordSignUp.requestFocus();
                    return;
                }

                if (Password2.isEmpty() || Password.length() < 6) {

                    if (language == 1) {
                        edtConfirmPasswordSignUp.setError("كلمة السر 6 عناصر");

                    } else if (language == 0) {
                        edtConfirmPasswordSignUp.setError("6 char password required");
                    }
                    edtConfirmPasswordSignUp.requestFocus();
                    return;
                }


                if (!Password.equals(Password2)) {


                    if (language == 1) {
                        alertDialog = new AlertDialog.Builder(VerifayEmailActivity.this);
                        alertDialog.setMessage("لايوجد تطابق بكلمة السر...");
                        alertDialog.show();

                    } else if (language == 0) {
                        alertDialog = new AlertDialog.Builder(VerifayEmailActivity.this);
                        alertDialog.setMessage("there isn't match ... ");
                        alertDialog.show();

                    }


                    return;
                }

                senEmail(code);
            }
        });




        if (language == 1) {

            edtEmailSignUp.setHint("البريد الالكتروني");
            edtPasswordSignUp.setHint("كلمة السر");
            edtConfirmPasswordSignUp.setHint("تأكيد كلمة السر");
            btnSignUp.setText("تسجيل الحساب");
        } else if (language == 0) {
            edtEmailSignUp.setHint("Email");
            edtPasswordSignUp.setHint("Password");
            edtConfirmPasswordSignUp.setHint("Confirm the Password");
            btnSignUp.setText("Signup");
        }
    }

    private void senEmail(final String code) {
        final String mEmail = edtEmailSignUp.getText().toString();
        final String mPassword = edtConfirmPasswordSignUp.getText().toString();


        JavaMailAPI javaMailAPI = new JavaMailAPI(this, mEmail, "Your Code is", code);

        javaMailAPI.execute();


        Dialog dialog = new Dialog(VerifayEmailActivity.this);
        dialog.setContentView(R.layout.verify_popup);

        dialog.setCanceledOnTouchOutside(false);

        final EditText etVerifyCode = dialog.findViewById(R.id.etVerifyCode);
        Button btnVerifyCode = dialog.findViewById(R.id.btnVerifyOTP);
        if (language == 1) {

            btnVerifyCode.setText("تحقق");
        } else if (language == 0) {
            btnVerifyCode.setText("Verify");

        }
        btnVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verificationCode = etVerifyCode.getText().toString();

                if (verificationCode.equals(code)) {
                    CreateNewUserActivity.email = mEmail;
                    CreateNewUserActivity.password = mPassword;
                    startActivity(new Intent(VerifayEmailActivity.this, CreateNewUserActivity.class));
                    finish();
                } else
                {
                    alertDialog = new AlertDialog.Builder(VerifayEmailActivity.this);

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        alertDialog.setMessage("هذا الكود خاطئ");
                    } else if (language == 0) {
                        alertDialog.setMessage("This code is error...");
                    }
                    alertDialog.show();

                }

            }
        });

        dialog.show();
    }
}
