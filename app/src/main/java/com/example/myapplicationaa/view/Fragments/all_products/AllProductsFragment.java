package com.example.myapplicationaa.view.Fragments.all_products;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.AllProductAdapter;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Offers;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.ProductORCategory;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.example.myapplicationaa.view.Fragments.order.OrderFragment;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class AllProductsFragment extends Fragment implements AllProductView {



    private RecyclerView recFrgProductAllProduct;
    private RecyclerView.LayoutManager layoutManager;
    public static AllProductAdapter allProductAdapter;
    public static List<Product> mProducts;

    public static Categories category;

    public static Offers offers;


    public static boolean ByCategory = false;

    public static boolean ByOffer = false;

    private FragmentActivity myContext;

    CircularProgressBar circularProgressBar;

    AllProductPresenter mPresenter;

    LinearLayout noItemsProducts;

    LinearLayout noWifiProduct;

    Button tryAgainProduct;

   TextView noInternetAllProduct;

    TextView  noItems;

    public SharedPreferences sp;


    public AllProductsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_products, container, false);

        sp = getActivity().getSharedPreferences("THIS", MODE_PRIVATE);

        initialize(view);



        noItemsProducts = view.findViewById(R.id.noItemsProducts);
        recFrgProductAllProduct.setVisibility(View.VISIBLE);
        noItemsProducts.setVisibility(View.GONE);


        int language = sp.getInt("language", 0);


        if (CategoriesFragment.ProductsByCategory) {
            mPresenter.getAllProduct(category.id);
            CategoriesFragment.ProductsByCategory = false;
            ByCategory = true;
           if (language == 1) {
                tryAgainProduct.setText("اعادة المحاولة");
                noInternetAllProduct.setText("لايوجد اتصال بلشبكة يرجى المحاولة");
                noItems.setText("لايوجد اي منتج لعرضه");

            } else if (language == 0) {
                tryAgainProduct.setText("Try again");
                noInternetAllProduct.setText("there isn't connection with internet");
                noItems.setText("there aren't any items");
            }
        }

        if (CategoriesFragment.ProductsByOffer) {
            category = new Categories();
            mPresenter.getProductByOffer(offers.getId());
            CategoriesFragment.ProductsByOffer = false;
            ByOffer = true;

           if (language == 1) {
                category.name_ar=(offers.detail_ar);
                tryAgainProduct.setText("اعادة المحاولة");
                noInternetAllProduct.setText("لايوجد اتصال بلشبكة يرجى المحاولة");
                noItems.setText("لايوجد اي منتج لعرضه");

            } else if (language == 0) {
                category.name_en=(offers.detail_en);
                tryAgainProduct.setText("Try again");
                noInternetAllProduct.setText("there isn't connection with internet");
                noItems.setText("there aren't any items");
            }

        }







        tryAgainProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).changeFragment(new CategoriesFragment());
            }
        });
        allProductAdapter = new AllProductAdapter(getContext(), mProducts, AllProductsFragment.this);

        return view;
    }

    public void changeFragment(Fragment fragment) {
        FragmentManager fragManager = myContext.getSupportFragmentManager();

        fragManager.beginTransaction().replace(R.id.frmDepartmentProduct, fragment).commit();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    public void initialize(View view) {
        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.all_products_progress_bar);
        circularProgressBar.setVisibility(View.GONE);
        noWifiProduct = view.findViewById(R.id.noWifiProduct);
        mPresenter = new AllProductPresenter(this);
        recFrgProductAllProduct = (RecyclerView) view.findViewById(R.id.recFrgProductAllProduct);
        recFrgProductAllProduct.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recFrgProductAllProduct.setLayoutManager(layoutManager);
        tryAgainProduct = view.findViewById(R.id.tryAgainProduct);
        noInternetAllProduct = view.findViewById(R.id.noInternetAllProduct);

        noItems= view.findViewById(R.id.noItems);
    }


    public void displayProductRecycler() {
        allProductAdapter = new AllProductAdapter(getContext(), mProducts, AllProductsFragment.this);
        recFrgProductAllProduct.setAdapter(allProductAdapter);
        allProductAdapter.notifyDataSetChanged();
    }


    @Override
    public void onShowLoading() {
        circularProgressBar.setVisibility(View.VISIBLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
        try {
            circularProgressBar.setVisibility(View.GONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (Exception e) {

        }

    }

    @Override
    public void onGetAllProductResult(ProductORCategory products) {



        if (products.getType().equals("product")) {
            mProducts = products.getData_product();
            displayProductRecycler();
        }

        if (products.getData_product().size() == 0) {
            recFrgProductAllProduct.setVisibility(View.GONE);
            noItemsProducts.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onGetAllProductError(String message) {

        recFrgProductAllProduct.setVisibility(View.GONE);
        noWifiProduct.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGetProductByOfferResult(List<Product> product) {
        mProducts = product;
        displayProductRecycler();
        Log.i("TAG", "onGetProductByOfferResult: " + product.size());

        if (product.size() == 0) {
            recFrgProductAllProduct.setVisibility(View.GONE);
            noItemsProducts.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetProductByOfferError(String message) {
        recFrgProductAllProduct.setVisibility(View.GONE);
        noWifiProduct.setVisibility(View.VISIBLE);
    }


    @Override
    public void onStart() {
        super.onStart();

        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            recFrgProductAllProduct.setVisibility(View.VISIBLE);
            noWifiProduct.setVisibility(View.GONE);
        } else {
            recFrgProductAllProduct.setVisibility(View.GONE);
            noWifiProduct.setVisibility(View.VISIBLE);
        }
    }


}
