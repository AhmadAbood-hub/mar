package com.example.myapplicationaa.view.Activites;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplicationaa.R;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {


    private TextView txtSelectLanguage;
    private Button btnSettingSave;
    private RadioButton rdArabic;
    private RadioButton rdEnglish;
    public SharedPreferences sp;
    public SharedPreferences.Editor edit;
    public static Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setSupportActionBar(toolbar);

        define();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }


        sp = SettingsActivity.this.getSharedPreferences("THIS", MODE_PRIVATE);
        edit = sp.edit();


        int language = sp.getInt("language", 0);

        int check = sp.getInt("check", 0);


        if (language == 1 && check == 1) {
            rdArabic.setChecked(true);
            rdEnglish.setChecked(false);
            txtSelectLanguage.setText("اختر لغة التطبيق");
            btnSettingSave.setText("حفظ");
            rdArabic.setText("عربي");
            rdEnglish.setText("انكليزي");
        } else if (language == 0 && check == 1) {
            rdArabic.setChecked(false);
            rdEnglish.setChecked(true);
            txtSelectLanguage.setText("Select app language");
            btnSettingSave.setText("Save");
            rdArabic.setText("Arabic");
            rdEnglish.setText("English");
        }


        btnSettingSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rdArabic.isChecked()) {
                    setLanguage(SettingsActivity.this, "ar");

                    startActivity(new Intent(SettingsActivity.this, MainActivity.class));

                    finish();
                    edit.putInt("language", 1);
                    edit.putInt("check", 1);
                    edit.apply();


                } else if (rdEnglish.isChecked()) {
                    setLanguage(SettingsActivity.this, "en");

                    startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                    SettingsActivity.this.finish();

                    edit.putInt("language", 0);
                    edit.putInt("check", 1);
                    edit.apply();

                }
            }
        });
    }

    public void setLanguage(Context c, String lang) {
        Locale localeNew = new Locale(lang);
        Locale.setDefault(localeNew);

        Resources res = c.getResources();
        Configuration newConfig = new Configuration(res.getConfiguration());
        newConfig.locale = localeNew;
        newConfig.setLayoutDirection(localeNew);
        res.updateConfiguration(newConfig, res.getDisplayMetrics());

        newConfig.setLocale(localeNew);
        c.createConfigurationContext(newConfig);
    }

    private void define() {

        txtSelectLanguage = findViewById(R.id.txtSelectLanguage);
        btnSettingSave = findViewById(R.id.btnSettingSave);
        rdArabic = findViewById(R.id.rdArabic);
        rdEnglish = findViewById(R.id.rdEnglish);
    }


}
