package com.example.myapplicationaa.view.Fragments.order;


import com.example.myapplicationaa.model.GetOrder;
import com.example.myapplicationaa.model.Order;
import com.example.myapplicationaa.model.Product;

import java.util.List;

public interface OrderView {
    void onShowLoading();
    void onHideLoading();
    void onGetOrdersResult(List<GetOrder> orders);
    void onGetOrdersError(String message);
}
