package com.example.myapplicationaa.view.Activites.create_new_user;

import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.model.UserValidate;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class CreateNewUserPresenter {
    CreateNewUserView view;
    CreateNewUserPresenter(CreateNewUserView view){this.view=view;}

    public void userRegister(String name,String email,String password,String first_name,
                          String last_name,String phone, String city,String prefecture,
                          String postcard,String country,String num_building,String num_street){
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<Register> call = apiInterface.createNewUser(name,"s","s",first_name,"s","s",last_name,"s","s",email,password,
        phone,city,prefecture,postcard,country,num_building,num_street,0);

        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
               view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetCreateUserResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                view.onHideLoading();
                view.onGetCreateUserError(t.getLocalizedMessage());
            }
        });
    }
}
