package com.example.myapplicationaa.view.Fragments.all_products;

import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.ProductORCategory;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllProductPresenter {
    AllProductView view;
    AllProductPresenter(AllProductView view){this.view=view;}

    public void getAllProduct(int category_id){
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        retrofit2.Call<ProductORCategory> call = apiInterface.getCategoryProductsOrSecondaryCategory(category_id);

        call.enqueue(new Callback<ProductORCategory>() {
            @Override
            public void onResponse(retrofit2.Call<ProductORCategory> call, Response<ProductORCategory> response) {
                view.onHideLoading();


                if (response.isSuccessful() && response.body() != null) {
                    view.onGetAllProductResult(response.body());
                }
            }

            public void onFailure(Call<ProductORCategory> call, Throwable t) {
                view.onHideLoading();
                view.onGetAllProductError(t.getLocalizedMessage());
            }
        });
    }


    public void getProductByOffer(int offer_id){
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        retrofit2.Call<List<Product>> call = apiInterface.getProductsByOffer(offer_id);

        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(retrofit2.Call<List<Product>> call, Response<List<Product>> response) {
                view.onHideLoading();


                if (response.isSuccessful() && response.body() != null) {
                    view.onGetProductByOfferResult(response.body());
                }
            }

            public void onFailure(Call<List<Product>> call, Throwable t) {
                view.onHideLoading();
                view.onGetProductByOfferError(t.getLocalizedMessage());
            }
        });
    }
}
