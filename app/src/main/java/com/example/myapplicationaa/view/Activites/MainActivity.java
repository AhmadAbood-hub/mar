package com.example.myapplicationaa.view.Activites;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.ui.AppBarConfiguration;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.MainProductAdapter;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.other.DailyAlarmReceiver;
import com.example.myapplicationaa.model.other.body;
import com.example.myapplicationaa.model.other.notification;
import com.example.myapplicationaa.view.Activites.login.LoginActivity;
import com.example.myapplicationaa.view.Fragments.notify.NorifyFragment;
import com.example.myapplicationaa.view.Fragments.order.OrderFragment;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.carts.CartFragment;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.example.myapplicationaa.view.Fragments.home.HomeFragment;
import com.example.myapplicationaa.view.Fragments.profile.ProfileFragment;
import com.example.myapplicationaa.view.Fragments.profile.ProfilePresenter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.ByCategory;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.ByOffer;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.allProductAdapter;
import static com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment.mProducts;
import static com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment.productAdapter;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.City;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Country;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Email;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.FirstName;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.LastName;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Name;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.NumBuilding;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.NumStreet;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Password;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Phone;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Postcode;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.Prefecture;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtCityUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtCountryUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtEmailUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtFirstNameUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtLastNameUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtNumBuildingUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtNumStreetUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtPasswordUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtPhoneUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtPostcodeUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtPrefectureUpdate;
import static com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.edtUserNameUpdate;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;

    public static boolean filter = false;

    public static boolean DetailsProductFragment = false;
    public static boolean DetailsProductFragment2 = false;
    public static boolean AllProductsFragment = false;
    public static boolean ProfileFragment = false;

    public MainProductAdapter mainProductAdapter;

    SharedPreferences sp;
    public SharedPreferences.Editor edit;

    public FirebaseAuth firebaseAuth;


    public HomeFragment homeFragment;

    public static BottomNavigationView bottomNav;

    public static MenuItem searchItem;
    public MenuItem checkItem;

    int language;

    boolean mExit = false;

    public static boolean mTap = false;

    private AlertDialog.Builder alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        registerAlarm(this, 1);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        setSupportActionBar(toolbar);


        sp = getSharedPreferences("THIS", MODE_PRIVATE);
        edit = sp.edit();

        mExit = false;

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_noun_hamburger_menu_2832820);


        bottomNav = findViewById(R.id.bottom_navigation);


        bottomNav.setOnNavigationItemSelectedListener(navListener);


        language = sp.getInt("language", 0);

        if (language == 1) {
            bottomNav.getMenu().findItem(R.id.home).setTitle("الرئيسية");
            bottomNav.getMenu().findItem(R.id.shopping).setTitle("السلة");
            bottomNav.getMenu().findItem(R.id.order).setTitle("الطلبات");
            bottomNav.getMenu().findItem(R.id.notify).setTitle("الاشعارات");

            navigationView.getMenu().findItem(R.id.nav_login).setTitle("تسجيل الدخول");
            navigationView.getMenu().findItem(R.id.nav_logout).setTitle("تسجيل الخروج");
            navigationView.getMenu().findItem(R.id.nav_profile).setTitle("البروفيل");
            navigationView.getMenu().findItem(R.id.nav_setting).setTitle("الاعدادت");
        } else if (language == 0) {
            bottomNav.getMenu().findItem(R.id.home).setTitle("Home");
            bottomNav.getMenu().findItem(R.id.shopping).setTitle("Shopping");
            bottomNav.getMenu().findItem(R.id.order).setTitle("Order");
            bottomNav.getMenu().findItem(R.id.notify).setTitle("Notification");


            navigationView.getMenu().findItem(R.id.nav_login).setTitle("Login");
            navigationView.getMenu().findItem(R.id.nav_logout).setTitle("Logout");
            navigationView.getMenu().findItem(R.id.nav_profile).setTitle("Profile");
            navigationView.getMenu().findItem(R.id.nav_setting).setTitle("Settings");

        }


        navigationView.setItemIconTintList(null);


        Fragment HomeFragment = new HomeFragment();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, HomeFragment).commit();

        homeFragment = new HomeFragment();


        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        searchItem = menu.findItem(R.id.action_search);

        searchItem.setVisible(false);

        checkItem = menu.findItem(R.id.action_check);

        checkItem.setVisible(false);

        View v = checkItem.getActionView();

        ImageView tv = v.findViewById(R.id.imgChk);

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int user_id = sp.getInt("user_id", 0);

                Name = edtUserNameUpdate.getText().toString();
                Email = edtEmailUpdate.getText().toString();
                Password = edtPasswordUpdate.getText().toString();
                FirstName = edtFirstNameUpdate.getText().toString();
                LastName = edtLastNameUpdate.getText().toString();
                Phone = edtPhoneUpdate.getText().toString();
                Country = edtCountryUpdate.getText().toString();
                Postcode = edtPostcodeUpdate.getText().toString();
                City = edtCityUpdate.getText().toString();
                NumBuilding = edtNumBuildingUpdate.getText().toString();
                NumStreet = edtNumStreetUpdate.getText().toString();
                Prefecture = edtPrefectureUpdate.getText().toString();


                com.example.myapplicationaa.view.Fragments.profile.ProfileFragment.mPresenter.updateProfileUserByID(user_id, Name, Email, Password, FirstName, LastName, Country,
                        Phone, City, Prefecture, Postcode, NumBuilding, NumStreet);
            }
        });

        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }

        try {
            searchView.setOnQueryTextListener(
                    new SearchView.OnQueryTextListener() {

                        // Override onQueryTextSubmit method
                        // which is call
                        // when submitquery is searched

                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            // If the list contains the search query
                            // than filter the adapter
                            // using the filter method
                            // with the query as its argument
                            if (mProducts.contains(query)) {
                                productAdapter.getFilter().filter(query);
                            } else {
                                // Search query not found in List View

                            }
                            return false;
                        }

                        // This method is overridden to filter
                        // the adapter according to a search query
                        // when the user is typing search
                        @Override
                        public boolean onQueryTextChange(String newText) {

                            if (!filter)

                                productAdapter.getFilter().filter(newText);


                            else
                                allProductAdapter.getFilter().filter(newText);

                            return false;
                        }
                    });
        } catch (Exception e) {

        }


        return super.onCreateOptionsMenu(menu);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem Item) {
            Fragment selectedFragment = null;
            switch (Item.getItemId()) {
                case R.id.home:
                    selectedFragment = new HomeFragment();
                    searchItem.setVisible(false);
                    break;
                case R.id.shopping:
                    selectedFragment = new CartFragment();
                    searchItem.setVisible(false);
                    break;
                case R.id.order:
                    selectedFragment = new OrderFragment();
                    searchItem.setVisible(false);
                    break;
                case R.id.notify:
                    selectedFragment = new NorifyFragment();
                    searchItem.setVisible(false);
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }

    };

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
////
////

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();

        if (id == R.id.nav_login) {
            Intent intent = new Intent(this, LoginActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_logout) {
            firebaseAuth.signOut();
            edit.putInt("user_id", 0);
            edit.apply();
//
        } else if (id == R.id.nav_profile) {
            int user_id = sp.getInt("user_id", 0);

            Log.i("TAG", "onNavigationItemSelected: " + user_id);

            if (user_id > 0) {
                ProfileFragment = true;
                searchItem.setVisible(false);
                checkItem.setVisible(true);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
            } else {
                alertDialog = new AlertDialog.Builder(MainActivity.this);
                if (language == 1) {
                    alertDialog.setMessage("يرجى تسجيل الدخول ...");
                } else if (language == 0) {
                    alertDialog.setMessage("please login ...");
                }
                alertDialog.show();
            }

        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(this, SettingsActivity.class));
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {

        if (DetailsProductFragment) {
            DetailsProductFragment = false;
            AllProductsFragment = true;
            searchItem.setVisible(true);
            checkItem.setVisible(false);
            showBottom();

            if (ByCategory) {
                CategoriesFragment.ProductsByCategory = true;
                ByCategory = false;
                checkItem.setVisible(false);

            }
            if (ByOffer) {
                CategoriesFragment.ProductsByOffer = true;
                ByOffer = false;
                checkItem.setVisible(false);

            }
            changeFragment(new AllProductsFragment());
        } else if (DetailsProductFragment2) {
            DetailsProductFragment2 = false;

            checkItem.setVisible(false);
            changeFragment(new CategoriesFragment());
            showBottom();
        } else if (AllProductsFragment) {

            checkItem.setVisible(false);
            changeFragment(new CategoriesFragment());
            AllProductsFragment = false;
            searchItem.setVisible(false);
            showBottom();
            checkItem.setVisible(false);

        } else if (ProfileFragment) {
            changeFragment(new CategoriesFragment());
            showBottom();
            ProfileFragment = false;
            searchItem.setVisible(false);
            checkItem.setVisible(false);

        } else if (mTap) {
            CategoriesFragment.pos = 0;
            changeFragment(new CategoriesFragment());
            mTap = false;
        } else {

          /*  if (mExit) {
                finish();
            } else {
                Toast.makeText(this, "اضغط رجوع مرة ثانية للخروج من التطبيق", Toast.LENGTH_SHORT).show();
                mExit = true;
            }*/
            finish();

        }
    }

    public void changeFragment(Fragment fragment) {

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

    }

    public static void showBottom() {
        bottomNav.setVisibility(View.VISIBLE);
    }

    public static void hideBottom() {
        bottomNav.setVisibility(View.GONE);
    }


    private void registerAlarm(Context context, int requestCode) {
        int HOUR = 60 * 60 * 1000;
        Intent intent = new Intent(context, DailyAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + 150 * HOUR, pendingIntent);
    }


}
