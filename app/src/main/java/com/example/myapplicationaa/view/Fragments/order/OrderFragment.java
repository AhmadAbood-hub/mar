package com.example.myapplicationaa.view.Fragments.order;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.OrderAdapter;
import com.example.myapplicationaa.adapter.OrderDetailsAdapter;
import com.example.myapplicationaa.model.GetOrder;
import com.example.myapplicationaa.model.Order;
import com.example.myapplicationaa.model.products;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class OrderFragment extends Fragment implements OrderView {

    public SharedPreferences sp;


    private RecyclerView recFrgOrders;
    private RecyclerView.LayoutManager layoutManager;
    private OrderAdapter orderAdapter;
    private List<GetOrder> orders;

    LinearLayout noWifiorder;
    Button tryAgainorder;

    TextView noInternetOrder;

    TextView txtNoOrder;


    private RecyclerView.LayoutManager layoutManagerDetails;
    private OrderDetailsAdapter orderDetailsAdapter;

    CircularProgressBar circularProgressBar;

    OrderPresenter mPresenter;

    LinearLayout noOrder;

    private FragmentActivity myContext;

    private AlertDialog.Builder alertDialog;

    View view;

    
    public OrderFragment() {
        // Required empty public constructor
    }
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order, container, false);

        noWifiorder = view.findViewById(R.id.noWifiorder);

        tryAgainorder = view.findViewById(R.id.tryAgainOrder);

        noInternetOrder = view.findViewById(R.id.noInternetOrder);

        txtNoOrder = view.findViewById(R.id.txtNoOrder);



        sp = getActivity().getSharedPreferences("THIS", MODE_PRIVATE);

        int user_id = sp.getInt("user_id", 0);



        noOrder = view.findViewById(R.id.noOrder);

        mPresenter = new OrderPresenter(OrderFragment.this);

        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.categories_progress_bar_order);
        circularProgressBar.setVisibility(View.GONE);


        recFrgOrders = (RecyclerView) view.findViewById(R.id.recFrgOrders);
        recFrgOrders.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recFrgOrders.setLayoutManager(layoutManager);
        orders = new ArrayList<>();
        orderAdapter = new OrderAdapter(getContext(), orders,  OrderFragment.this);

        Log.i("TAG", "onCreateView: "+user_id);

        mPresenter.getOrdersByID(user_id);




        tryAgainorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               changeFragment(new OrderFragment());
            }
        });



        int language = sp.getInt("language", 0);

        if (language == 1) {
            tryAgainorder.setText("اعادة المحاولة");
            noInternetOrder.setText("لايوجد اتصال بلشبكة يرجى المحاولة");
            txtNoOrder.setText(" لايوجد أي طلبية حتى الأن");


        } else if (language == 0) {
            tryAgainorder.setText("Try again");
            noInternetOrder.setText("there isn't connection with internet");
            txtNoOrder.setText("there isn't any order yet");

        }



        return view;
    }


    @Override
    public void onShowLoading() {

//        recFrgOrders.setBackgroundResource(R.color.green_opacity);
        circularProgressBar.setVisibility(View.VISIBLE);
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
//                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
        try {
//            recFrgOrders.setBackgroundResource(R.color.colorWhite);
            circularProgressBar.setVisibility(View.GONE);
//            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (Exception e) {

        }

    }

    @Override
    public void onGetOrdersResult(List<GetOrder> orders) {

        this.orders = orders;


        orderAdapter = new OrderAdapter(getContext(), orders,  OrderFragment.this);
        recFrgOrders.setAdapter(orderAdapter);


        if (orders.size() > 0) {

            recFrgOrders.setVisibility(View.VISIBLE);


        } else {

            recFrgOrders.setVisibility(View.GONE);

            noOrder.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onGetOrdersError(String message) {

        noWifiorder.setVisibility(View.VISIBLE);
    }

    public void showDetailsProduct(List<products> products)
    {
        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.order_item_popup);

        final TextView subject = dialog.findViewById(R.id.subject);
        final TextView quantity = dialog.findViewById(R.id.quantity);
        final TextView price = dialog.findViewById(R.id.price);

        int language = sp.getInt("language", 0);

        if (language == 1) {
            subject.setText("المادة");
            quantity.setText("الكمية");
            price.setText("السعر");
        } else if (language == 0) {
            subject.setText("Subject");
            quantity.setText("Quantity");
            price.setText("Price");
        }


        final RecyclerView recyclerDetailsOrder = dialog.findViewById(R.id.recyclerDetailsOrder);
        recyclerDetailsOrder.setHasFixedSize(true);
        layoutManagerDetails = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerDetailsOrder.setLayoutManager(layoutManagerDetails);
        orderDetailsAdapter = new OrderDetailsAdapter(getContext(), products);

        recyclerDetailsOrder.setAdapter(orderDetailsAdapter);
        orderDetailsAdapter.setData(products);

        if (products.size()>0)
        dialog.show();
        else
        {

            if (language == 1) {
                alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("لايوجد عناصر لعرضها...");
                alertDialog.show();

            } else if (language == 0) {
                alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("there aren't any items... ");
                alertDialog.show();

            }

        }

    }

    public void changeFragment(Fragment fragment) {
        FragmentManager fragManager = myContext.getSupportFragmentManager();

        fragManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

}
