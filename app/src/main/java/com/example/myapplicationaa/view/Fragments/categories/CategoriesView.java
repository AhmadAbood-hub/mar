package com.example.myapplicationaa.view.Fragments.categories;


import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Offers;
import com.example.myapplicationaa.model.ProductORCategory;

import java.util.List;

public interface CategoriesView {
    void onShowLoading();
    void onHideLoading();



    void onGetCategoryOfferResult(List<Offers> offers);
    void onGetCategoryOfferError(String message);

    void onGetCategoryProductResult(ProductORCategory offers);
    void onGetCategoryProductError(String message);

    void onGetAllCategoriesResult(List<Categories> categories);
    void onGetAllCategoriesError(String message);

    void onGetAllOffersResult(List<Offers> categories);
    void onGetAllOffersError(String message);

    void onGetCategoryEssentialResult(List<Categories> categories);
    void onGetCategoryEssentialError(String message);
}
