package com.example.myapplicationaa.view.Fragments.carts;

import android.util.Log;
import android.widget.Toast;

import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.Order;
import com.example.myapplicationaa.model.Point;
import com.example.myapplicationaa.model.other.OrderResponse;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myapplicationaa.view.Fragments.carts.CartFragment.mUrlOrder;

public class CartPresenter {

    CartsView view;

    CartPresenter(CartsView view) {
        this.view = view;
    }

    public void addOrders() {
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<OrderResponse> call = apiInterface.addOrder(mUrlOrder);

     //   Order?user_id=70&order_date=2020/5/5&products[0]={"id":"1","quantity":"5"}&products[1]={"id":"1","quantity":"5"}&products[2]={"id":"1","quantity":"5"}&products[3]={"id":"1","quantity":"5"}&products[4]={"id":"1","quantity":"5"}

        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetOrdersResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                view.onHideLoading();
                view.onGetOrdersError(t.getLocalizedMessage());
            }
        });
    }

    public void getMinimumPriceForOrder(){
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<List<Point>> call = apiInterface.getMinimumPriceForOrder();

        call.enqueue(new Callback<List<Point>>() {
            @Override
            public void onResponse(Call<List<Point>> call, Response<List<Point>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetLimitOrderResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Point>> call, Throwable t) {
                view.onHideLoading();
                view.onGetLimitOrderError(t.getLocalizedMessage());
            }
        });
    }

}
