package com.example.myapplicationaa.view.Fragments.order;

import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.GetOrder;
import com.example.myapplicationaa.model.Order;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPresenter {

    OrderView view;
    OrderPresenter(OrderView view){this.view=view;}
    public  void getOrdersByID(int id){
                view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<List<GetOrder>> call = apiInterface.getOrdersByUserID(id);

        call.enqueue(new Callback<List<GetOrder>>() {
            @Override
            public void onResponse(Call<List<GetOrder>> call, Response<List<GetOrder>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetOrdersResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<GetOrder>> call, Throwable t) {
                view.onHideLoading();
                view.onGetOrdersError(t.getLocalizedMessage());
            }
        });
    }

}
