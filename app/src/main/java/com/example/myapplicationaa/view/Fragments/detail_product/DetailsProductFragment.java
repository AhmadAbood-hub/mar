package com.example.myapplicationaa.view.Fragments.detail_product;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.Components;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.other.Cart;
import com.example.myapplicationaa.model.other.DataBase;
import com.example.myapplicationaa.model.other.body;
import com.example.myapplicationaa.model.other.notification;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;
import static com.example.myapplicationaa.model.other.DataBase.CreateDB;
import static com.example.myapplicationaa.model.other.DataBase.TAG;
import static com.example.myapplicationaa.view.Activites.MainActivity.DetailsProductFragment2;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.category;


public class DetailsProductFragment extends Fragment implements DetailsProductView {

    private LinearLayout LinearIngredients;

    public static Product mainProduct;

    private TextView mDiscount;
    private TextView mOldPrice;
    private TextView mNewPrice;

    private TextView mNameDepartment;
    private TextView mNameProduct;

    private TextView mIDProduct;
    private TextView mDescraptionProduct;
    private TextView mIngredients;
    private TextView mIngredientsQuantity;
    private TextView mNutritionalValues;
    private TextView mDetailsProduct;
    private TextView mConnact;
    private TextView mOther;

    private ImageView mGoToDescraptionProduct;
    private ImageView mGoToIngredients;
    private ImageView mGoToNutritionalValues;
    private ImageView mGoToDetailsProduct;
    private ImageView mGoToConnact;
    private ImageView mGoToOther;


    private ImageView mImageProduct;
    private ImageView mAddToCart;
    private ImageView mAddToFavorite;

    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;

    private FragmentActivity myContext;

    ArrayList<Cart> all_data;

    CircularProgressBar circularProgressBar;

    DetailsProductPresenter mPresenter;


    private AlertDialog.Builder alertDialog;

    public SharedPreferences sp;


    View view;

    public DetailsProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_details_product, container, false);

        sp = getActivity().getSharedPreferences("THIS", MODE_PRIVATE);


        mPresenter = new DetailsProductPresenter(DetailsProductFragment.this);

        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.categories_progress_bar_details_product);
        circularProgressBar.setVisibility(View.GONE);


        CreateDB(getActivity());


        LinearIngredients = view.findViewById(R.id.LinearIngredients);

        mAddToFavorite = view.findViewById(R.id.imgFavorite);
        mIDProduct = view.findViewById(R.id.txtIDProduct);
        mNameProduct = view.findViewById(R.id.txtNameProduct);
        mNameDepartment = view.findViewById(R.id.txtNameDepartment);
        mImageProduct = view.findViewById(R.id.imgImageProduct);
        mDiscount = view.findViewById(R.id.txtDiscount);
        mOldPrice = view.findViewById(R.id.txtOldPrice);
        mNewPrice = view.findViewById(R.id.txtNewPrice);
        mDescraptionProduct = view.findViewById(R.id.txtDescraptionProduct);
        mIngredients = view.findViewById(R.id.txtIngredients);
        mIngredientsQuantity = view.findViewById(R.id.txtIngredientsQuantity);

        mNutritionalValues = view.findViewById(R.id.txtNutritionalValues);
        mDetailsProduct = view.findViewById(R.id.txtDetailsProduct);
        mConnact = view.findViewById(R.id.txtConnact);
        mOther = view.findViewById(R.id.txtOther);


        mGoToDescraptionProduct = view.findViewById(R.id.imgDescraptionProduct);
        mGoToIngredients = view.findViewById(R.id.imgIngredients);
        mGoToNutritionalValues = view.findViewById(R.id.imgNutritionalValues);
        mGoToDetailsProduct = view.findViewById(R.id.imgDetailsProduct);
        mGoToConnact = view.findViewById(R.id.imgConnact);
        mGoToOther = view.findViewById(R.id.imgOther);

        mAddToCart = view.findViewById(R.id.imgAddCart);


        mAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    all_data = new ArrayList<>();
                    SQLiteDatabase db = getActivity().openOrCreateDatabase(DataBase.dbName, MODE_PRIVATE, null);
                    Cursor cursor = db.rawQuery("select * from product ", null);
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        for (int i = 0; i < cursor.getCount(); i++) {
                            Cart c = new Cart();
                            c.IDProductCart = cursor.getInt(cursor.getColumnIndex("IDProduct"));
                            all_data.add(c);
                            cursor.moveToNext();
                        }
                    }
                } catch (Exception ex) {
                }

                for (Cart cart : all_data) {
                    Log.i(TAG, "onClick: " + mainProduct.name_en);
                    if (cart.IDProductCart == mainProduct.id) {
                        f = true;
                        break;
                    }

                }

                if (!f) {
                    DataBase.ExecuteNonQuery(getActivity(), "insert into product (nameProduct,nameArProduct,nameGrProduct,IDProduct,totalPrice,oldPriceProduct,newPriceProduct,imgProduct,sizeProduct) values ('" + mainProduct.name_en + "','" +
                            mainProduct.name_ar + "','" + "germany" +
                            "','" + mainProduct.id + "','" + mainProduct.price_after + "'," + mainProduct.price_befor + "," + mainProduct.price_after + "" +
                            ",'" + mainProduct.picture + "'," + 1 + ")");

                    alertDialog = new AlertDialog.Builder(getActivity());


                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        alertDialog.setMessage("تمت الاضافة بنجاح ...");

                    } else if (language == 0) {
                        alertDialog.setMessage("Added successfully ...");

                    }
                    alertDialog.show();

                    f = true;

                } else {
                    alertDialog = new AlertDialog.Builder(getActivity());




                    alertDialog = new AlertDialog.Builder(getActivity());


                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        alertDialog.setMessage("هذا العنصر مضاف سابقا...");


                    } else if (language == 0) {
                        alertDialog.setMessage("This item was previously added ...");

                    }
                    alertDialog.show();


                }

            }
        });


        mGoToDescraptionProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!a) {
                    mGoToDescraptionProduct.setBackgroundResource(R.drawable.ic_up);
                    mDescraptionProduct.setVisibility(View.VISIBLE);
                    a = true;
                } else {
                    mGoToDescraptionProduct.setBackgroundResource(R.drawable.ic_down);
                    mDescraptionProduct.setVisibility(View.GONE);

                    a = false;
                }
            }
        });

        mGoToIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!b) {
                    mGoToIngredients.setBackgroundResource(R.drawable.ic_up);
                    LinearIngredients.setVisibility(View.VISIBLE);
                    b = true;
                } else {
                    mGoToIngredients.setBackgroundResource(R.drawable.ic_down);
                    LinearIngredients.setVisibility(View.GONE);

                    b = false;
                }

            }
        });

        mGoToNutritionalValues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!c) {
                    mGoToNutritionalValues.setBackgroundResource(R.drawable.ic_up);
                    mNutritionalValues.setVisibility(View.VISIBLE);
                    c = true;
                } else {
                    mGoToNutritionalValues.setBackgroundResource(R.drawable.ic_down);
                    mNutritionalValues.setVisibility(View.GONE);

                    c = false;
                }
            }
        });

        mGoToDetailsProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!d) {
                    mGoToDetailsProduct.setBackgroundResource(R.drawable.ic_up);
                    mDetailsProduct.setVisibility(View.VISIBLE);
                    d = true;
                } else {
                    mGoToDetailsProduct.setBackgroundResource(R.drawable.ic_down);
                    mDetailsProduct.setVisibility(View.GONE);
                    d = false;
                }
            }
        });

        mGoToConnact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!e) {
                    mGoToConnact.setBackgroundResource(R.drawable.ic_up);
                    mConnact.setVisibility(View.VISIBLE);
                    e = true;
                } else {
                    mGoToConnact.setBackgroundResource(R.drawable.ic_down);
                    mConnact.setVisibility(View.GONE);
                    e = false;
                }
            }
        });

        mGoToOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!f) {
                    mGoToOther.setBackgroundResource(R.drawable.ic_up);
                    mOther.setVisibility(View.VISIBLE);
                    f = true;
                } else {
                    mGoToOther.setBackgroundResource(R.drawable.ic_down);
                    mOther.setVisibility(View.GONE);
                    f = false;
                }
            }
        });


        StrikeOldPrice();


        setDataToPage(mainProduct);

        mPresenter.getComponentsByProduct(mainProduct.id);


        return view;
    }


    @SuppressLint("SetTextI18n")
    public void setDataToPage(Product product) {
        if (product.discount == 0)
            mDiscount.setVisibility(View.GONE);

        int language = sp.getInt("language", 0);

        if (language == 1) {
            mNameProduct.setText( product.name_ar);
            mDescraptionProduct.setText(product.description_ar);
            mDetailsProduct.setText(product.detail_ar);
            mConnact.setText(product.communication_ar);
            mNutritionalValues.setText(product.alimentrly_value_ar);
            mOther.setText(product.other_ar);
            mOldPrice.setText(String.valueOf(product.getPrice_befor()));
            if (product.discount > 0)
                mNewPrice.setText(String.valueOf(product.getPrice_after() - ((product.getPrice_after() * product.discount) / 100)));
            else
                mNewPrice.setText(String.valueOf(product.getPrice_after()));

            mIDProduct.setText(getResources().getString(R.string.product_id) + " : " + product.getId());
            mNameDepartment.setText( category.name_ar);
            mDiscount.setText("خصم" + " : " + product.discount + "%");
            Picasso.get().load(product.getPicture()).into(mImageProduct);
        } else if (language == 0) {
            mNameProduct.setText(  product.getName_en());
            mDescraptionProduct.setText(product.getDescription_en());
            mDetailsProduct.setText(product.getDetail_en());
            mConnact.setText(product.getCommunication_en());
            mNutritionalValues.setText(product.getAlimentrly_value_en());
            mOther.setText(product.getOther_en());
            mOldPrice.setText(String.valueOf(product.getPrice_befor()));
            if (product.discount > 0)
                mNewPrice.setText(String.valueOf(product.getPrice_after() - ((product.getPrice_after() * product.discount) / 100)));
            else
                mNewPrice.setText(String.valueOf(product.getPrice_after()));

            mIDProduct.setText(getResources().getString(R.string.product_id) + " : " + product.getId());
            mNameDepartment.setText( category.name_en);
            mDiscount.setText("Discount" + " : " + product.discount + "%");
            Picasso.get().load(product.getPicture()).into(mImageProduct);
        }


    }

    private void StrikeOldPrice() {
        SpannableString ss = new SpannableString(mOldPrice.getText().toString());
        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();

        String aa = mOldPrice.getText().toString();
        int s = aa.length();
        ss.setSpan(strikethroughSpan, 0, s, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mOldPrice.setText(ss);
    }


    @Override
    public void onShowLoading() {
        circularProgressBar.setVisibility(View.VISIBLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
        try {
            circularProgressBar.setVisibility(View.GONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (Exception e) {

        }
    }

    @Override
    public void onGetComponentsByProductResult(List<Components> components) {
        StringBuilder Ingredients = new StringBuilder();

        for (int i = 0; i < components.size(); i++) {
            int language = sp.getInt("language", 0);

            if (language == 1) {
                Ingredients.append(components.get(i).name_ar);
            } else if (language == 0) {
                Ingredients.append(components.get(i).name_en);
            }

            if (i != components.size() - 1)
                Ingredients.append("\n");
        }

        mIngredients.setText(Ingredients);


        StringBuilder IngredientsQuantity = new StringBuilder();

        for (int i = 0; i < components.size(); i++) {
            IngredientsQuantity.append(components.get(i).quantity + " g");
            if (i != components.size() - 1)
                IngredientsQuantity.append("\n");
        }

        mIngredientsQuantity.setText(IngredientsQuantity);

    }

    @Override
    public void onGetComponentsByProductError(String message) {

    }
}
