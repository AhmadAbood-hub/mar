package com.example.myapplicationaa.view.Fragments.categories;


import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Offers;
import com.example.myapplicationaa.model.ProductORCategory;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesPresenter {
    CategoriesView view;

    CategoriesPresenter(CategoriesView view) {
        this.view = view;
    }

    public void getAllCategories() {
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        retrofit2.Call<List<Categories>> call = apiInterface.getAllCategories();

        call.enqueue(new Callback<List<Categories>>() {
            @Override
            public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetAllCategoriesResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Categories>> call, Throwable t) {
                view.onHideLoading();

                view.onGetAllCategoriesError(t.getLocalizedMessage());
            }
        });
    }

    public void getAllOffers() {
        view.onShowLoading();
        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        retrofit2.Call<List<Offers>> call = apiInterface.getAllOffers();

        call.enqueue(new Callback<List<Offers>>() {
            @Override
            public void onResponse(Call<List<Offers>> call, Response<List<Offers>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetAllOffersResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Offers>> call, Throwable t) {
                view.onHideLoading();
                view.onGetAllOffersError(t.getLocalizedMessage());

            }
        });
    }

    public void getAllDepartment() {

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<List<Categories>> call = apiInterface.getAllEssentialCategories();

        call.enqueue(new Callback<List<Categories>>() {
            @Override
            public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetCategoryEssentialResult(response.body());
                }
            }

            public void onFailure(Call<List<Categories>> call, Throwable t) {
                view.onGetCategoryEssentialError(t.getLocalizedMessage());
                view.onHideLoading();

            }
        });
    }

    public void getCategoryOffer(int category_id) {

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<List<Offers>> call = apiInterface.getCategoryOffers(category_id);

        call.enqueue(new Callback<List<Offers>>() {
            @Override
            public void onResponse(Call<List<Offers>> call, Response<List<Offers>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {

                    view.onGetCategoryOfferResult(response.body());
                }
            }

            public void onFailure(Call<List<Offers>> call, Throwable t) {
                view.onHideLoading();
                view.onGetCategoryOfferError(t.getLocalizedMessage());
            }
        });
    }

    public void getCategoryProduct(int category_id) {
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<ProductORCategory> call = apiInterface.getCategoryProductsOrSecondaryCategory(category_id);

        call.enqueue(new Callback<ProductORCategory>() {
            @Override
            public void onResponse(Call<ProductORCategory> call, Response<ProductORCategory> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetCategoryProductResult(response.body());
                }
            }

            public void onFailure(Call<ProductORCategory> call, Throwable t) {
                view.onHideLoading();

                view.onGetCategoryProductError(t.getLocalizedMessage());
            }
        });
    }


}
