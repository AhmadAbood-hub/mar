package com.example.myapplicationaa.view.Activites.login;



import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;

import com.example.myapplicationaa.model.UserValidate;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {
    LoginView view;
    LoginPresenter(LoginView view){this.view=view;}

    public void userLogin(String email,String password){
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        retrofit2.Call<UserValidate> call = apiInterface.LoginUser(email,password);

        call.enqueue(new Callback<UserValidate>() {
            @Override
            public void onResponse(Call<UserValidate> call, Response<UserValidate> response) {
                view.onHideLoading();
                if (response.isSuccessful() && response.body() != null) {
                    view.onGetLoginResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserValidate> call, Throwable t) {
                view.onHideLoading();
                view.onGetLoginError(t.getLocalizedMessage());
            }
        });
    }
}
