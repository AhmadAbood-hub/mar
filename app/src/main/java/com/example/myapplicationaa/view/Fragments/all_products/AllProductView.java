package com.example.myapplicationaa.view.Fragments.all_products;

import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.ProductORCategory;

import java.util.List;

public interface AllProductView {
    void onShowLoading();
    void onHideLoading();
    void onGetAllProductResult(ProductORCategory products);
    void onGetAllProductError(String message);
    void onGetProductByOfferResult(List<Product> products);
    void onGetProductByOfferError(String message);
}
