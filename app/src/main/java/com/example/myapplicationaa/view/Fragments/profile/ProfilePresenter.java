package com.example.myapplicationaa.view.Fragments.profile;

import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.EditUser;
import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter {

    ProfileView view;

    ProfilePresenter(ProfileView view) {
        this.view = view;
    }

    public void getProfileUserByID(int id) {
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<User> call = apiInterface.getUserProfileByID(id);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetProfileResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.onHideLoading();
                view.onGetProfileError(t.getLocalizedMessage());
            }
        });
    }


    public void updateProfileUserByID(int id, String name, String email, String password, String first_name, String last_name, String country,
                                      String phone, String city, String prefecture, String postcard, String num_building, String num_street) {
        view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<EditUser> call = apiInterface.UserEdit(id, name,"d","d", email, password, first_name,"d","d", last_name,"d","d", country, phone,
                city, prefecture, postcard, num_building, num_street,1
        );

        call.enqueue(new Callback<EditUser>() {
            @Override
            public void onResponse(Call<EditUser> call, Response<EditUser> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onUpdateProfileResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<EditUser> call, Throwable t) {
                view.onHideLoading();
                view.onUpdateProfileError();
            }
        });
    }

}
