package com.example.myapplicationaa.view.Fragments.carts;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.CartAdapter;
import com.example.myapplicationaa.model.Order;
import com.example.myapplicationaa.model.Point;
import com.example.myapplicationaa.model.other.Cart;
import com.example.myapplicationaa.model.other.DataBase;
import com.example.myapplicationaa.model.other.OrderResponse;
import com.example.myapplicationaa.view.Activites.CreditCardActivity;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class CartFragment extends Fragment implements CartsView {
    public static Button btnConfirmOrder;
    public static RecyclerView recFrgCarts;
    private RecyclerView.LayoutManager layoutManager;
    public static CartAdapter cartAdapter;
    private List<Cart> carts;
    SeekBar seekBar;
    View thumbView;
    private FragmentActivity myContext;
    public static ArrayList<Cart> all_data;
    public static CartPresenter mPresenter;
    ProgressDialog progressDialog;
    public static LinearLayout noCart;
    CircularProgressBar circularProgressBar;
    LinearLayout mLayout;
    RelativeLayout mRelativeLayout;
    public static TextView mTxtTotalPrice;
    public static TextView mTxtTextTotlal;
    public SharedPreferences sp;
    public static String mUrlOrder;
    private AlertDialog.Builder alertDialog;
    private static final int PAYPAL_REQUEST_CODE = 7171;
    int totalOrderPrice;
    int limitPriceForOrder;
    TextView mLimitOrder,mLimitOrderNote;
    private CardView cardMinimumPrice;
    private CardView cardTotalPrice;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(DataBase.PAYPAL_CLIENT_ID);
    Dialog dialog;
    int language;
    TextView noCartItem;
    View view;
    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,

                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_carts, container, false);

        noCartItem = view.findViewById(R.id.noCartItem);
        cardMinimumPrice=(CardView)view.findViewById(R.id.cardMinimumPrice);
        cardTotalPrice=(CardView)view.findViewById(R.id.aa);
        cardTotalPrice.setVisibility(View.GONE);
        cardMinimumPrice.setVisibility(View.GONE);

        Intent intent = new Intent(getContext(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);

        sp = getActivity().getSharedPreferences("THIS", MODE_PRIVATE);
        language = sp.getInt("language", 0);
        mUrlOrder = "Order?";

        mLimitOrder=(TextView)view.findViewById(R.id.text_limit_order);
        mLimitOrderNote=(TextView)view.findViewById(R.id.text_limit_order_note);
        mPresenter = new CartPresenter(CartFragment.this);
        thumbView = LayoutInflater.from(getContext())
                .inflate(R.layout.layout_seekbar_thumb, null, false);

        seekBar = (SeekBar) view.findViewById(R.id.cart_seek_bar);
        seekBar.setThumb(getThumb(0));
        seekBar.setProgress(0);
        seekBar.setEnabled(false);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                // You can have your own calculation for progress
                seekBar.setThumb(getThumb(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mTxtTotalPrice = (TextView) view.findViewById(R.id.txtTotalPrice);
        mTxtTextTotlal = (TextView) view.findViewById(R.id.txtTextTotlal);


        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.categories_progress_bar_cart);
        circularProgressBar.setVisibility(View.GONE);

        mLayout = view.findViewById(R.id.rr);

        mRelativeLayout = view.findViewById(R.id.ee);


        noCart = view.findViewById(R.id.noCart);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("please wait");

        btnConfirmOrder = view.findViewById(R.id.btnConfirmOrder);

        recFrgCarts = (RecyclerView) view.findViewById(R.id.recFrgCarts);
        recFrgCarts.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL
                , false);
        recFrgCarts.setLayoutManager(layoutManager);
        carts = new ArrayList<>();
        cartAdapter = new CartAdapter(getContext(), carts, CartFragment.this);


        try {

            all_data = new ArrayList<>();
            SQLiteDatabase db = getActivity().openOrCreateDatabase(DataBase.dbName, MODE_PRIVATE,
                    null);
            Cursor cursor = db.rawQuery("select * from product ", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount(); i++) {
                    Cart c = new Cart();
                    c.IDProductCart = cursor.getInt(cursor.getColumnIndex("IDProduct"));
                    c.totalPrice = cursor.getInt(cursor.getColumnIndex("totalPrice"));
                    c.nameProductCart = cursor.getString(cursor.getColumnIndex("nameProduct"));
                    c.nameArProductCart = cursor.getString(cursor.getColumnIndex("nameArProduct"));
                    c.nameGrProductCart = cursor.getString(cursor.getColumnIndex("nameGrProduct"));
                    c.oldPriceCart = String.valueOf(cursor.getInt(cursor.getColumnIndex("oldPriceProduct")));
                    c.newPriceCart = String.valueOf(cursor.getInt(cursor.getColumnIndex("newPriceProduct")));
                    c.imgProductCart = cursor.getString(cursor.getColumnIndex("imgProduct"));
                    c.sizeCart = cursor.getInt(cursor.getColumnIndex("sizeProduct"));
                    all_data.add(c);
                    cursor.moveToNext();
                }
            }

           updateTotalPrice();
            if (language == 1) {

                noCartItem.setText("لايوجد أي مواد مختارة");
            } else if (language == 0) {

                noCartItem.setText("there aren't any selected item");
            }

            cartAdapter.setData(all_data);
            recFrgCarts.setAdapter(cartAdapter);

        }
        catch (Exception ex) { }

        btnConfirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                all_data = new ArrayList<>();
                SQLiteDatabase db = getActivity().openOrCreateDatabase(DataBase.dbName
                        , MODE_PRIVATE, null);
                Cursor cursor = db.rawQuery("select * from product ", null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    for (int i = 0; i < cursor.getCount(); i++) {
                        Cart c = new Cart();
                        c.IDProductCart = cursor.getInt(cursor.getColumnIndex("IDProduct"));
                        c.totalPrice = cursor.getInt(cursor.getColumnIndex("totalPrice"));
                        c.nameProductCart = cursor.getString(cursor.getColumnIndex("nameProduct"));
                        c.nameArProductCart = cursor.getString(cursor.getColumnIndex("nameArProduct"));
                        c.nameGrProductCart = cursor.getString(cursor.getColumnIndex("nameGrProduct"));
                        c.oldPriceCart = String.valueOf(cursor.getInt(cursor.getColumnIndex("oldPriceProduct")));
                        c.newPriceCart = String.valueOf(cursor.getInt(cursor.getColumnIndex("newPriceProduct")));
                        c.imgProductCart = cursor.getString(cursor.getColumnIndex("imgProduct"));
                        c.sizeCart = cursor.getInt(cursor.getColumnIndex("sizeProduct"));
                        all_data.add(c);
                        cursor.moveToNext();
                    }
                }

                String saveCurrentDate;

                Calendar calendar = Calendar.getInstance();

                SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
                saveCurrentDate = currentDate.format(calendar.getTime());

                int user_id = sp.getInt("user_id", 0);

                if (user_id != 0) {
                    mUrlOrder += "user_id=" + user_id + "&order_date=" + saveCurrentDate + "";

                    for (int i = 0; i < all_data.size(); i++) {

                        Log.i("TAG", "onClick: " + all_data.get(i).IDProductCart);
                        mUrlOrder += "&products[" + i + "]={\"id\":\""
                                + all_data.get(i).IDProductCart + "\",\"quantity\":\""
                                + all_data.get(i).sizeCart + "\"}";
                    }
                    int X = Integer.parseInt(DataBase.ExecuteScalar(getActivity(),
                            "select sum(totalPrice) from product"));
                    mUrlOrder += "&total=" + X + "";
                    dialog = new Dialog(getContext());
                    dialog.setContentView(R.layout.payment_popup);
                    final ImageView mPaypal = dialog.findViewById(R.id.imgPaypal);
                    mPaypal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int X = Integer.parseInt(DataBase.ExecuteScalar(getActivity(),
                                    "select sum(totalPrice) from product"));

                            dialog.dismiss();
                            processPayment(X);
                        }
                    });

                    final ImageView mCreditCard = dialog.findViewById(R.id.imgCreditCard);
                    mCreditCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                dialog.dismiss();
                                CreditCardActivity.amount = X;
                                startActivity(new Intent(getActivity(), CreditCardActivity.class));
                            }
                        }
                    });


                    dialog.show();


                } else {

                    alertDialog = new AlertDialog.Builder(getActivity());
                    if (language == 1) {
                        alertDialog.setMessage("يرجى تسجيل الدخول...");
                    } else if (language == 0) {
                        alertDialog.setMessage("Please login");
                    }
                    alertDialog.show();
                }


            }
        });
        mPresenter.getMinimumPriceForOrder();
        btnConfirmOrder.setVisibility(View.GONE);
        cardMinimumPrice.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }


    @Override
    public void onShowLoading() {
//        mRelativeLayout.setBackgroundResource(R.color.green_opacity);
        mLayout.setVisibility(View.GONE);
        circularProgressBar.setVisibility(View.VISIBLE);
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
//                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
        mLayout.setVisibility(View.VISIBLE);
//        mRelativeLayout.setBackgroundResource(R.color.colorWhite);
        circularProgressBar.setVisibility(View.GONE);
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    public void onGetOrdersResult(OrderResponse order) {
        if (order.success) {
            alertDialog = new AlertDialog.Builder(getActivity());

            if (language == 1) {
                alertDialog.setMessage("تم طلبك بنجاح");
            } else if (language == 0) {
                alertDialog.setMessage("Your order is done");
            }
            alertDialog.show();
        } else {
            alertDialog = new AlertDialog.Builder(getActivity());

            if (language == 1) {
                alertDialog.setMessage("فشل الطلب");
            } else if (language == 0) {
                alertDialog.setMessage("Your order is failed");
            }
            alertDialog.show();
        }
    }

    @Override
    public void onGetOrdersError(String message) {
        //   Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onGetLimitOrderResult(List<Point> point) {

         limitPriceForOrder=Integer.parseInt(point.get(0).getValue())+1000;
         seekBar.setMax(limitPriceForOrder);
         mLimitOrder.setText(String.valueOf(limitPriceForOrder)+"€");
    }

    @Override
    public void onGetLimitOrderError(String message) {
       Log.d("RRTR",message);
    }


    public void updateTotalPrice() {

         totalOrderPrice = Integer.parseInt(DataBase.ExecuteScalar(getActivity()
                 , "select sum(totalPrice) from product"));

        if (all_data.size() > 0) {
            seekBar.setThumb(getThumb(totalOrderPrice));
            seekBar.setProgress(totalOrderPrice);
            cardMinimumPrice.setVisibility(View.VISIBLE);
            recFrgCarts.setVisibility(View.VISIBLE);

            int language = sp.getInt("language", 0);
            if (language == 1) {
                btnConfirmOrder.setText("تأكيد الطلب");
                cardTotalPrice.setVisibility(View.VISIBLE);
                mTxtTotalPrice.setText(totalOrderPrice + "€" + "");
                mTxtTextTotlal.setText("المجموع");
            } else
                if (language == 0) {
                btnConfirmOrder.setText("Confirm your order");
                mTxtTotalPrice.setText(totalOrderPrice + "€" + "");
                cardTotalPrice.setVisibility(View.VISIBLE);
                mTxtTextTotlal.setText("Total");
            }

            if (totalOrderPrice>=limitPriceForOrder){
                btnConfirmOrder.setVisibility(View.VISIBLE);
                cardMinimumPrice.setVisibility(View.GONE);
//                mLimitOrder.setVisibility(View.GONE);
//                mLimitOrderNote.setVisibility(View.GONE);
            }else {
                btnConfirmOrder.setVisibility(View.GONE);
                cardMinimumPrice.setVisibility(View.VISIBLE);
//                mLimitOrder.setVisibility(View.VISIBLE);
//                mLimitOrderNote.setVisibility(View.VISIBLE);
            }

        }
        else {
            cardMinimumPrice.setVisibility(View.GONE);
//            mLimitOrderNote.setVisibility(View.GONE);
//            mLimitOrder.setVisibility(View.GONE);
            recFrgCarts.setVisibility(View.GONE);
            btnConfirmOrder.setVisibility(View.GONE);
            noCart.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onStart() {
        super.onStart();
    }


    private void processPayment(int amount) {
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "USD",
                "TestApp", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    public void onDestroy() {
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                PaymentConfirmation confirmation = data.getParcelableExtra
                        (PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null) {
                    paymentDone();

                }
            }
        }

    }

    @SuppressLint("NewApi")
    public Drawable getThumb(int progress) {
        ((TextView) thumbView.findViewById(R.id.tv_progress)).setText(progress + "$");
        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(getResources(), bitmap);
    }

    @SuppressLint("NewApi")
    public void updateSeekBar(int value) {
        seekBar.setThumb(getThumb(value));
        seekBar.setProgress(value);
    }


    public void paymentDone() {
        mPresenter.addOrders();

        DataBase.ExecuteNonQuery(getActivity(), "delete from product");
        all_data.clear();
        cartAdapter.setData(all_data);

        cardMinimumPrice.setVisibility(View.GONE);

        btnConfirmOrder.setVisibility(View.GONE);

       cardTotalPrice.setVisibility(View.GONE);

        recFrgCarts.setVisibility(View.GONE);

        noCart.setVisibility(View.VISIBLE);
    }


}
