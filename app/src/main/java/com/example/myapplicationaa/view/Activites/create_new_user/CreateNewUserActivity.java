package com.example.myapplicationaa.view.Activites.create_new_user;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Activites.VerifayEmailActivity;
import com.example.myapplicationaa.view.Activites.login.LoginActivity;
import com.example.myapplicationaa.view.Activites.login.LoginPresenter;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;


public class CreateNewUserActivity extends AppCompatActivity implements CreateNewUserView {

    public static String email;
    public static String password;

    private TextView EmailInfo;
    private TextView UserInfo;
    private TextView LocationInfo;


    private EditText edtUserName;
    private EditText edtEmail;
    private EditText edtPassword;
    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtPhone;
    private EditText edtPrefecture;
    private EditText edtCountry;
    private EditText edtPostcode;
    private EditText edtCity;
    private EditText edtNumBuilding;
    private EditText edtNumStreet;

    String UserName;
    String Email;
    String Password;
    String FirstName;
    String LastName;
    String Phone;
    String Prefecture;
    String Country;
    String Postcode;
    String City;
    String NumBuilding;
    String NumStreet;


    private Button btnCreate;

    CircularProgressBar circularProgressBar;


    CreateNewUserPresenter mPresenter;

    ProgressDialog progressDialog;


    ScrollView mScrollView;

    public SharedPreferences sp;
    public SharedPreferences.Editor edit;

    private AlertDialog.Builder alertDialog;

    //////////////

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_user);


        sp = getSharedPreferences("THIS", MODE_PRIVATE);
        edit = sp.edit();

        mScrollView = findViewById(R.id.ss);

        circularProgressBar = (CircularProgressBar) findViewById(R.id.categories_progress_bar_new_user);
        circularProgressBar.setVisibility(View.GONE);

        progressDialog = new ProgressDialog(CreateNewUserActivity.this);
        progressDialog.setMessage("please wait");


        edtUserName = findViewById(R.id.edtUserName);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtFirstName = findViewById(R.id.edtFirstName);
        edtLastName = findViewById(R.id.edtLastName);
        edtPhone = findViewById(R.id.edtPhone);
        edtPrefecture = findViewById(R.id.edtPrefecture);
        edtCountry = findViewById(R.id.edtCountry);
        edtPostcode = findViewById(R.id.edtPostcode);
        edtCity = findViewById(R.id.edtCity);
        edtNumBuilding = findViewById(R.id.edtNumBuilding);
        edtNumStreet = findViewById(R.id.edtNumStreet);
        btnCreate = findViewById(R.id.btnCreate);

        EmailInfo = findViewById(R.id.EmailInfo);
        LocationInfo = findViewById(R.id.LocationInfo);
        UserInfo = findViewById(R.id.UserInfo);


        RelativeLayout myLayout = (RelativeLayout) findViewById(R.id.create_user_relative);


        mPresenter = new CreateNewUserPresenter(this);


        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);

        edtEmail.setText(email);
        edtPassword.setText(password);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserName = edtUserName.getText().toString();
                Password = edtPassword.getText().toString();
                Email = edtEmail.getText().toString();
                FirstName = edtFirstName.getText().toString();
                LastName = edtLastName.getText().toString();
                Phone = edtPhone.getText().toString();
                Prefecture = edtPrefecture.getText().toString();
                Country = edtCountry.getText().toString();
                Postcode = edtPostcode.getText().toString();
                City = edtCity.getText().toString();
                NumBuilding = edtNumBuilding.getText().toString();
                NumStreet = edtNumStreet.getText().toString();


                if (UserName.isEmpty()) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtUserName.setError("اسم المستخدم مطلوب");

                    } else if (language == 0) {
                        edtUserName.setError("UserName Requierd");
                    }
                    edtUserName.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtEmail.setError("مطلوب بريد الكتروني صحيح");
                    } else if (language == 0) {
                        edtEmail.setError("Vaild Email Requierd");
                    }
                    edtEmail.requestFocus();
                    return;
                }

                if (Email.isEmpty()) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtEmail.setError("مطلوب البريد الاكتروني");
                    } else if (language == 0) {
                        edtEmail.setError("Email Requierd");

                    }
                    edtEmail.requestFocus();
                    return;
                }

                if (Password.isEmpty() || Password.length() < 6) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtPassword.setError("مطلوب 6 رموز");
                    } else if (language == 0) {
                        edtPassword.setError("6 char password required");
                    }
                    edtPassword.requestFocus();
                    return;
                }


                if (FirstName.isEmpty()) {

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtFirstName.setError("مطلوب الأسم الأول");
                    } else if (language == 0) {
                        edtFirstName.setError("FirstName Requierd");
                    }

                    edtFirstName.requestFocus();
                    return;
                }

                if (LastName.isEmpty()) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtLastName.setError("مطلوب الأسم الأخير");
                    } else if (language == 0) {
                        edtLastName.setError("LastName Requierd");
                    }
                    edtLastName.requestFocus();
                    return;
                }
                if (Phone.isEmpty()) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtPhone.setError("مطلوب رقم الهاتف");
                    } else if (language == 0) {
                        edtPhone.setError("Phone Requierd");
                    }

                    edtPhone.requestFocus();
                    return;
                }
                if (Prefecture.isEmpty()) {
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtPrefecture.setError("مطلوب المقاطعة");
                    } else if (language == 0) {
                        edtPrefecture.setError("Prefecture Requierd");
                    }
                    edtPrefecture.requestFocus();
                    return;
                }
                if (Country.isEmpty()) {

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtCountry.setError("مطلوب الدولة");
                    } else if (language == 0) {
                        edtCountry.setError("Country Requierd");
                    }

                    edtCountry.requestFocus();
                    return;
                }
                if (Postcode.isEmpty()) {

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtPostcode.setError("مطلوب رمز المدينة");
                    } else if (language == 0) {
                        edtPostcode.setError("Postcode Requierd");
                    }

                    edtPostcode.requestFocus();
                    return;
                }
                if (City.isEmpty()) {

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtCity.setError("مطلوب المدينة");
                    } else if (language == 0) {
                        edtCity.setError("City Requierd");
                    }

                    edtCity.requestFocus();
                    return;
                }
                if (NumBuilding.isEmpty()) {

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtNumBuilding.setError("مطلوب رقم المبنى");
                    } else if (language == 0) {
                        edtNumBuilding.setError("NumBuilding Requierd");
                    }


                    edtNumBuilding.requestFocus();
                    return;
                }
                if (NumStreet.isEmpty()) {

                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        edtNumStreet.setError("مطلوب رقم الشارع");
                    } else if (language == 0) {
                        edtNumStreet.setError("NumStreet Requierd");
                    }

                    edtNumStreet.requestFocus();
                    return;
                }


                mPresenter.userRegister(UserName, Email, Password,
                        FirstName, LastName, Phone,
                        City, Prefecture, Postcode, Country,
                        NumBuilding, NumStreet
                );


            }
        });

        int language = sp.getInt("language", 0);

        if (language == 1) {
            UserInfo.setText("معلومات المستخدم");
            LocationInfo.setText("معلومات الموقع");
            EmailInfo.setText("معلومات البريد الالكتروني");

            edtUserName.setHint("اسم المستخدم");
            edtEmail.setHint("البريد الالكتروني");
            edtCity.setHint("المدينة");
            edtCountry.setHint("الدولة");
            edtFirstName.setHint("الأسم الأول");
            edtLastName.setHint("الأسم الأخير");
            edtNumBuilding.setHint("رقم المبنى");
            edtNumStreet.setHint("رقم الشارع");
            edtPassword.setHint("كلمة السر");
            edtPostcode.setHint("رمز المدينة");
            edtPrefecture.setHint("المقاطعة");
            edtPhone.setHint("الهاتف");

            btnCreate.setText("انشاء الحساب");

        } else if (language == 0) {
            UserInfo.setText("User Info");
            LocationInfo.setText("Location Info");
            EmailInfo.setText("Email Info");

            edtUserName.setHint("UserName");
            edtEmail.setHint("Email");
            edtCity.setHint("City");
            edtCountry.setHint("Country");
            edtFirstName.setHint("FirstName");
            edtLastName.setHint("LastName");
            edtNumBuilding.setHint("NumBuilding");
            edtNumStreet.setHint("NumStreet");
            edtPassword.setHint("Password");
            edtPostcode.setHint("Postcode");
            edtPrefecture.setHint("Prefecture");
            edtPhone.setHint("Phone");

            btnCreate.setText("Create Account");

        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CreateNewUserActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onShowLoading() {

//        mScrollView.setBackgroundResource(R.color.green_opacity);
        circularProgressBar.setVisibility(View.VISIBLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
//                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
//        mScrollView.setBackgroundResource(R.color.colorWhite);
        circularProgressBar.setVisibility(View.GONE);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onGetCreateUserResult(Register user) {
        if (user.getSuccess() == true) {
            edit.putInt("user_id", user.getMessage().id);
            edit.apply();

            alertDialog = new AlertDialog.Builder(CreateNewUserActivity.this);
            alertDialog.setMessage("User Has Inserted");
            alertDialog.show();


            Intent intent = new Intent(CreateNewUserActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            alertDialog = new AlertDialog.Builder(CreateNewUserActivity.this);
            alertDialog.setMessage("فشلت عملية اضافة حساب");
            alertDialog.show();
        }
    }

    @Override
    public void onGetCreateUserError(String message) {
        alertDialog = new AlertDialog.Builder(CreateNewUserActivity.this);
        alertDialog.setMessage("فشلت عملية اضافة حساب");
        alertDialog.show();
    }
}
