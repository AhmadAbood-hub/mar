package com.example.myapplicationaa.view.Fragments.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.CategoriesTabAdapter;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.other.CategoryTab;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements HomeView {


    private FragmentActivity myContext;


    ProgressDialog progressDialog;

    CategoriesFragment categoriesFragment = new CategoriesFragment();

    HomePresenter mPresenter;


    public HomeFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home
                , container, false);


        mPresenter = new HomePresenter(this);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("please wait");


        changeFragment(categoriesFragment, getActivity());


        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }


    public void changeFragment(Fragment fragment, FragmentActivity myContext) {
        FragmentManager fragManager = myContext.getSupportFragmentManager();

        fragManager.beginTransaction().replace(R.id.frmDepartmentProduct, fragment).commit();
    }

    public void changeFragment1(Fragment fragment) {
        FragmentManager fragManager = myContext.getSupportFragmentManager();

        fragManager.beginTransaction().replace(R.id.frmDepartmentProduct, fragment).commit();
    }

    @Override
    public void onShowLoading() {
        progressDialog.show();
    }

    @Override
    public void onHideLoading() {
        progressDialog.hide();
    }


}
