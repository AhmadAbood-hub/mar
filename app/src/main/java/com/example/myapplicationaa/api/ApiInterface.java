package com.example.myapplicationaa.api;


import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Components;
import com.example.myapplicationaa.model.EditUser;
import com.example.myapplicationaa.model.GetOrder;
import com.example.myapplicationaa.model.Offers;
import com.example.myapplicationaa.model.Point;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.ProductORCategory;
import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.model.User;
import com.example.myapplicationaa.model.UserValidate;
import com.example.myapplicationaa.model.other.OrderResponse;
import com.example.myapplicationaa.model.other.body;
import com.example.myapplicationaa.view.Fragments.carts.CartFragment;

import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET("CategoriesEssential")
    Call<List<Categories>> getAllEssentialCategories();

    @GET("Categories")
    Call<List<Categories>> getAllCategories();

    @GET("Offers")
    Call<List<Offers>> getAllOffers();

    @GET("ProductsByCategorySecondary")
    Call<ProductORCategory> getCategoryProductsOrSecondaryCategory
            (@Query("category_id") int category_id);

    @GET("ProductsByOffer")
    Call<List<Product>> getProductsByOffer(
            @Query("offer_id") int offer_id
    );

    @GET("ComponentsByProduct")
    Call<List<Components>> getComponentsByProduct(
            @Query("product_id") int product_id
    );


    @GET("OffersByCategory")
    Call<List<Offers>> getCategoryOffers(
            @Query("category_id") int category_id
    );


    @GET("OrderProductByUser")
    Call<List<GetOrder>> getOrdersByUserID(
            @Query("user_id") int user_id
    );


    @GET("UserProfile")
    Call<User> getUserProfileByID(
            @Query("user_id") int user_id
    );

    @FormUrlEncoded
    @POST("Register")
    Call<Register> createNewUser(
            @Field("name_en") String name_en,
            @Field("name_ar") String name_ar,
            @Field("name_gr") String name_gr,

            @Field("first_name_en") String first_name_en,
            @Field("first_name_ar") String first_name_ar,
            @Field("first_name_gr") String first_name_gr,

            @Field("last_name_en") String last_name_en,
            @Field("last_name_ar") String last_name_ar,
            @Field("last_name_gr") String last_name_gr,


            @Field("email") String email,
            @Field("password") String password,
            @Field("phone") String phone,
            @Field("city") String city,
            @Field("prefecture") String prefecture,
            @Field("postcard") String postcard,
            @Field("country") String country,
            @Field("num_building") String num_building,
            @Field("num_street") String num_street,
            @Field("points") int points
    );

    @POST()
    Call<OrderResponse> addOrder(@Url String url);


    @GET("Login")
    Call<UserValidate> LoginUser(
            @Query("email") String email,
            @Query("password") String password
    );


    @GET("alter-user")
    Call<EditUser> UserEdit(
            @Query("id") int id,

            @Query("name_en") String name_en,
            @Query("name_ar") String name_ar,
            @Query("name_gr") String name_gr,

            @Query("email") String email,
            @Query("password") String password,

            @Query("first_name_en") String first_name_en,
            @Query("first_name_ar") String first_name_ar,
            @Query("first_name_gr") String first_name_gr,

            @Query("last_name_en") String last_name_en,
            @Query("last_name_ar") String last_name_ar,
            @Query("last_name_gr") String last_name_gr,

            @Query("country") String country,
            @Query("phone") String phone,
            @Query("city") String city,
            @Query("prefecture") String prefecture,
            @Query("postcard") String postcard,
            @Query("num_building") String num_building,
            @Query("num_street") String num_street,
            @Query("points") int points
    );


    @Headers({
            "Content-Type: application/json",
            "Authorization: key=AAAACPuIhk4:APA91bGSd_mgAVLmdk96tfakcziIhApTvN1HdYFNSWWMlWVo_jTRh5Ku235GxK-iem34h_QqB05D8wGIJLjsnNAZV99qx0xECfgRWctp_cVv6kWwiEGeI4jh9aiVCWmXhyFh8vb2hH7i"
    })

    @POST("fcm/send")
    Call<ResponseBody> createNotification(@Body body body);

    @GET("numberPoint")
    Call<List<Point>> getPointNumberForEveryEuro();

    @GET("limitOrder")
    Call<List<Point>> getMinimumPriceForOrder();

    @GET("limitPoint")
    Call<List<Point>> getLimitPointToTransForm();


}
