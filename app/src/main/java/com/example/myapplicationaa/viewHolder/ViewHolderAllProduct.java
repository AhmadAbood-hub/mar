package com.example.myapplicationaa.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationaa.R;


public class ViewHolderAllProduct extends RecyclerView.ViewHolder {


    private View view;

    private CardView mCard;
    private TextView txtProductItemNameProduct;
    private TextView txtProductItemDescriptionProduct;

    private TextView txtProductItemNewPriceProduct;
    private TextView txtProductItemOldPriceProduct;


    private ImageView imgProductItemImageProduct;


    public ViewHolderAllProduct(@NonNull View itemView) {
        super(itemView);

        this.view = itemView;

        mCard=(CardView)view.findViewById(R.id.crdAllProduct);

        txtProductItemNameProduct = (TextView) view.findViewById(R.id.txtProductItemNameProduct);

        txtProductItemDescriptionProduct = (TextView) view.findViewById(R.id.txtProductItemDescriptionProduct);

        txtProductItemNewPriceProduct = (TextView) view.findViewById(R.id.txtProductItemNewPriceProduct);

        txtProductItemOldPriceProduct = (TextView) view.findViewById(R.id.txtProductItemOldPriceProduct);

        imgProductItemImageProduct = (ImageView) view.findViewById(R.id.imgProductItemImageProduct);

//        imgProductItemGoToDetailesProduct = (ImageView) view.findViewById(R.id.imgProductItemGoToDetailesProduct);

    }


    public View getView() {
        return view;
    }


    public TextView getTxtProductItemNameProduct() {
        return txtProductItemNameProduct;
    }

    public TextView getTxtProductItemDescriptionProduct() {
        return txtProductItemDescriptionProduct;
    }

    public TextView getTxtProductItemNewPriceProduct() {
        return txtProductItemNewPriceProduct;
    }

    public TextView getTxtProductItemOldPriceProduct() {
        return txtProductItemOldPriceProduct;
    }

    public ImageView getImgProductItemImageProduct() {
        return imgProductItemImageProduct;
    }



}
