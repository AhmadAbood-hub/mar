package com.example.myapplicationaa.viewHolder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;

public class ViewHolderOrder extends RecyclerView.ViewHolder {


    private View view;

    private TextView txtNameOrder;
    private TextView txtDateOrder;
    private TextView txtTotalPriceOrder;
    private ImageButton btnViewOrder;



    public ViewHolderOrder(@NonNull View itemView) {
        super(itemView);

        this.view = itemView;

        txtDateOrder = (TextView) view.findViewById(R.id.txtDateOrder);
        txtTotalPriceOrder = (TextView) view.findViewById(R.id.txtTotalPriceOrder);
        btnViewOrder = (ImageButton) view.findViewById(R.id.btnViewOrder);



    }


    public View getView() {
        return view;
    }


    public TextView getTxtNameOrder() {
        return txtNameOrder;
    }

    public TextView getTxtDateOrder() {
        return txtDateOrder;
    }

    public TextView getTxtTotalPriceOrder() {
        return txtTotalPriceOrder;
    }

    public ImageButton getBtnViewOrder() {
        return btnViewOrder;
    }

}
