package com.example.myapplicationaa.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.CategoriesAdapter;


public class ViewHolderCategory extends RecyclerView.ViewHolder   {


    private View view;

    private TextView txtDepartment;

    private ImageView ImgDepartment;

    private CardView mCard;

    public ViewHolderCategory(@NonNull View itemView) {
        super(itemView);

        this.view = itemView;

        txtDepartment = (TextView) view.findViewById(R.id.txtDepartment);
        ImgDepartment = (ImageView) view.findViewById(R.id.ImgDepartment);
        mCard = (CardView) view.findViewById(R.id.category_card);



    }


    public View getView() {
        return view;
    }

    public TextView getTxtDepartment() {
        return txtDepartment;
    }

    public ImageView getImgDepartment() {
        return ImgDepartment;
    }

    public CardView getLinDepartment() {
        return mCard;
    }


}

