package com.example.myapplicationaa.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.other.CustomFilter;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.example.myapplicationaa.view.Fragments.detail_product.DetailsProductFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.ByCategory;

public class MainProductAdapter extends RecyclerView.Adapter<MainProductAdapter.MainProductViewHolder> implements Filterable {
    Context context;
    public List<Product> mProducts, mProductsFilter;
    private Fragment fragment;
    CustomFilter filter;
    public SharedPreferences sp;

    public MainProductAdapter(Context context, List<Product> mProducts, Fragment fragment) {
        this.context = context;
        this.mProducts = mProducts;
        this.fragment = fragment;
        mProductsFilter = mProducts;
    }

    public MainProductAdapter( List<Product> mProducts) {

        this.mProducts = mProducts;

    }

    @NonNull
    @Override
    public MainProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.main_product_item, parent, false);
        return new MainProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainProductViewHolder holder, final int position) {
        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);

        final Product product = mProducts.get(position);

        int language = sp.getInt("language", 0);

        if (language == 1) {
            holder.mText.setText(product.name_ar);

        } else if (language == 0) {
            holder.mText.setText(product.name_en);
        }


        Picasso.get().load(product.picture).into(holder.mImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoriesFragment.ProductsByCategory = true;
                MainActivity.DetailsProductFragment2 = true;
                MainActivity.filter = true;



                DetailsProductFragment.mainProduct = product;
                ((MainActivity) context).changeFragment(new DetailsProductFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilter((ArrayList<Product>) mProductsFilter, this,context);

        }
        return filter;
    }

    class MainProductViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        ImageView mImage;
        TextView mText;
        CardView mCard;
        ItemClickListener itemClickListener;

        public MainProductViewHolder(@NonNull View itemView) {
            super(itemView);
            mCard = (CardView) itemView.findViewById(R.id.main_product_card);
            mImage = (ImageView) itemView.findViewById(R.id.main_product_image);
            mText = (TextView) itemView.findViewById(R.id.main_product_text);
            mCard.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public void setData(List<Product> products) {
        this.mProducts = products;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
