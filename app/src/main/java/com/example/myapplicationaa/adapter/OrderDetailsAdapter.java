package com.example.myapplicationaa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.GetOrder;
import com.example.myapplicationaa.model.products;
import com.example.myapplicationaa.viewHolder.ViewHolderOrder;
import com.example.myapplicationaa.viewHolder.ViewHolderOrderDetails;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class OrderDetailsAdapter extends RecyclerView.Adapter<ViewHolderOrderDetails> {
    private Context context;
    private List<products> products;
    public SharedPreferences sp;

    public OrderDetailsAdapter(Context context, List<products> products) {
        this.context = context;
        this.products = products;
    }

    @NonNull
    @Override
    public ViewHolderOrderDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_details_item, parent, false);
        return new ViewHolderOrderDetails(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderOrderDetails viewHolderOrderDetails, final int position) {

        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);

        int language = sp.getInt("language", 0);



        final products product = products.get(position);

        viewHolderOrderDetails.getTxtIDItemOrder().setText(product.price_buy+"€");

        viewHolderOrderDetails.getTxtQuantityItemOrder().setText(product.quantity+"");

        if (language == 1) {
            viewHolderOrderDetails.getTxtNameItemOrder().setText(product.name_ar);
        } else if (language == 0) {
            viewHolderOrderDetails.getTxtNameItemOrder().setText(product.name_en);
        }


    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setData(List<products> products) {
        this.products = products;
        notifyDataSetChanged();
    }


}
