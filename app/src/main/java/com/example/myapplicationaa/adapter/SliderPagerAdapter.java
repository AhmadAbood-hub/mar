package com.example.myapplicationaa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.Offers;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.ByCategory;

public class SliderPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<Offers> mList;


    public SliderPagerAdapter(Context mContext, List<Offers> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {


        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View slideLayout = inflater.inflate(R.layout.slide_item, null);

        ImageView slideImg = slideLayout.findViewById(R.id.slide_img);
        Picasso.get().load(mList.get(position).getPicture()).into(slideImg);

        slideImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AllProductsFragment.offers = mList.get(position);
                CategoriesFragment.ProductsByOffer = true;
                MainActivity.AllProductsFragment = true;
                CategoriesFragment.ProductsByCategory = false;
                ByCategory = false;
                MainActivity.searchItem.setVisible(true);
                MainActivity.filter = true;
                MainActivity.DetailsProductFragment = false;
                ((MainActivity) mContext).changeFragment(new AllProductsFragment());

            }
        });


        container.addView(slideLayout);
        return slideLayout;


    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
