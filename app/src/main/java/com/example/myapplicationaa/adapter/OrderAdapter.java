package com.example.myapplicationaa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.GetOrder;
import com.example.myapplicationaa.model.Order;
import com.example.myapplicationaa.view.Fragments.order.OrderFragment;
import com.example.myapplicationaa.viewHolder.ViewHolderOrder;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<ViewHolderOrder> {
    private Context context;
    private List<GetOrder> Orders;
    private Fragment fragment;



    public OrderAdapter(Context context, List<GetOrder> Orders, Fragment fragment) {
        this.context = context;
        this.Orders = Orders;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public ViewHolderOrder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        return new ViewHolderOrder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderOrder ViewHolderOrder, final int position) {

        final GetOrder Order = Orders.get(position);


        ViewHolderOrder.getTxtDateOrder().setText(Order.order_data);

        ViewHolderOrder.getTxtTotalPriceOrder().setText(Order.total+"€");


        ViewHolderOrder.getBtnViewOrder().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((OrderFragment)fragment).showDetailsProduct(Order.products);
            }
        });

    }

    @Override
    public int getItemCount() {
        return Orders.size();
    }


    public void setData(List<GetOrder> Orders) {
        this.Orders = Orders;
        notifyDataSetChanged();
    }


}
